import Vapor

/// Register your application's routes here.
public func routes(_ router: Router) throws {
    // Basic "It works" example
    router.get { req -> String in
        let loggedResponseHeaders = try req.http.headers
        return "Request \(loggedResponseHeaders)"
    }
    
    // Basic "Hello, world!" example
    router.get("hello", String.parameter) { req -> String in
        let name = try req.parameters.next(String.self)
        let parms = try req.http.headers
        return "hello \(name) these are the headers \(parms)"
    }

    router.post(InfoData.self, at: "info") {req, data -> InfoResponse in
        return InfoResponse(request: data)
    }

    router.post("api" , "CloudServices") { req -> Future<CloudService> in
        return try req.content.decode(CloudService.self)
            .flatMap(to: CloudService.self) { CloudService in
        return CloudService.save(on: req)
        }
    }
    
}

struct InfoData: Content {
    let name: String
}

struct InfoResponse: Content {
    let request: InfoData
}

