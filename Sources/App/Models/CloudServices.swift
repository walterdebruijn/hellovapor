import Vapor
import FluentSQLite

final class CloudService: Codable{
    var id: Int?
    var short: String
    var long: String
    
    init(short: String, long: String) {
        self.short = short
        self.long = long
        
    }
}

extension CloudService: SQLiteModel {}
extension CloudService: Migration {}
extension CloudService: Content {}


